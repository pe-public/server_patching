# @summary
#   A class to deploy patching-related tools
#
# At the moment only includes the class `server_patching::validate`.
#
# @example
#   include server_patching
#
class server_patching {
  include server_patching::validate
}
