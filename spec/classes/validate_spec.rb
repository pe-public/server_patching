# frozen_string_literal: true

require 'spec_helper'

describe 'server_patching::validate' do
  on_supported_os(get_distros).each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }
      let(:params) do
        {
          validation_script: '/usr/local/bin/validate.sh'
        }
      end

      context 'with ensure => absent' do
        let :params do
          super().merge({ 'ensure' => 'absent' })
        end

        context 'with no checks defined' do
          it {
            is_expected.to contain_file('/usr/local/bin/validate.sh').with_ensure('absent')
            is_expected.not_to contain_concat('/usr/local/bin/validate.sh')
          }
        end

        context 'with a checks defined' do
          let :params do
            super().merge({ 'services' => [
                            {
                              'name' => 'nginx.service',
                              'active' => true,
                            },
                          ], })
          end

          it { is_expected.to contain_concat('/usr/local/bin/validate.sh').with_ensure('absent') }
        end

        it {
          is_expected.not_to contain_concat__fragment('bash-header')
          is_expected.not_to contain_concat__fragment('bash-footer')
          is_expected.not_to contain_concat__fragment('validate-services')
          is_expected.not_to contain_concat__fragment('validate-processes')
          is_expected.not_to contain_concat__fragment('validate-ports')
          is_expected.not_to contain_concat__fragment('validate-urls')
          is_expected.not_to contain_concat__fragment('validate-mounts')
          is_expected.not_to contain_concat__fragment('validate-exports')
          is_expected.not_to contain_concat__fragment('validate-zpools')
        }
      end

      context 'with ensure => present' do
        let :params do
          super().merge({ 'ensure' => 'present' })
        end

        it {
          is_expected.to contain_file('/usr/local/bin/validate.sh').with_ensure('absent')
          is_expected.not_to contain_concat('/usr/local/bin/validate.sh')
        }

        context 'with services' do
          let :params do
            super().merge({ 'services' => [ { 'name' => 'nginx.service' }] })
          end

          it {
            is_expected.to contain_concat('/usr/local/bin/validate.sh').with_ensure('present')
            is_expected.to contain_concat__fragment('bash-header')
            is_expected.to contain_concat__fragment('bash-footer')
            is_expected.to contain_concat__fragment('validate-services').with_content(%r{systemctl is-active nginx.service})
            is_expected.to contain_concat__fragment('validate-services').with_content(%r{ \$status == "active" })
          }

          context 'with an active service' do
            let :params do
              super().merge({ 'services' => [
                              {
                                'name' => 'nginx.service',
                                'active' => true,
                              },
                            ], })
            end

            it {
              is_expected.to contain_concat__fragment('validate-services').with_content(%r{ \$status == "active" })
            }
          end

          context 'with a failed service' do
            let :params do
              super().merge({ 'services' => [
                              {
                                'name' => 'nginx.service',
                                'active' => false,
                              },
                            ], })
            end

            it {
              is_expected.to contain_concat__fragment('validate-services').with_content(%r{\$status == "failed"})
            }
          end
        end

        context 'with ports' do
          let :params do
            super().merge({ 'ports' => [{ 'port': 443 }] })
          end

          it {
            is_expected.to contain_concat('/usr/local/bin/validate.sh').with_ensure('present')
            is_expected.to contain_concat__fragment('bash-header')
            is_expected.to contain_concat__fragment('bash-footer')
            is_expected.to contain_concat__fragment('validate-ports').with_content(%r{":443 "})
            is_expected.to contain_concat__fragment('validate-ports').with_content(%r{--inet})
            is_expected.to contain_concat__fragment('validate-ports').with_content(%r{-t})
          }

          context 'with an active port' do
            let :params do
              super().merge({ 'ports' => [
                              {
                                'port':   443,
                                'proto':  'tcp',
                                'ip_ver': 'ipv4',
                                'listening': true,
                              },
                            ], })
            end

            it {
              is_expected.to contain_concat__fragment('validate-ports').with_content(%r{\$ports -gt 0})
            }
          end

          context 'with an inactive port' do
            let :params do
              super().merge({ 'ports' => [
                              {
                                'port':   443,
                                'proto':  'tcp',
                                'ip_ver': 'ipv4',
                                'listening': false,
                              },
                            ], })
            end

            it {
              is_expected.to contain_concat__fragment('validate-ports').with_content(%r{\$ports -eq 0})
            }
          end

          context 'with an ipv6 udp port' do
            let :params do
              super().merge({ 'ports' => [
                              {
                                'port':   443,
                                'proto':  'udp',
                                'ip_ver': 'ipv6',
                                'listening': false,
                              },
                            ], })
            end

            it {
              is_expected.to contain_concat__fragment('validate-ports').with_content(%r{-u})
              is_expected.to contain_concat__fragment('validate-ports').with_content(%r{--inet6})
            }
          end
        end

        context 'with urls' do
          let :params do
            super().merge({ 'urls' => [
                            {
                              'url':     'https://www.google.com',
                              'status':  200,
                            },
                          ], })
          end

          it {
            is_expected.to contain_concat('/usr/local/bin/validate.sh').with_ensure('present')
            is_expected.to contain_concat__fragment('bash-header')
            is_expected.to contain_concat__fragment('bash-footer')
            is_expected.to contain_concat__fragment('validate-urls').with_content(%r{https:\/\/www\.google\.com})
            is_expected.to contain_concat__fragment('validate-urls').with_content(%r{\$status -eq 200})
          }
        end

        context 'with processes' do
          let :params do
            super().merge({ 'processes' => [
                            {
                              'name':     'ntpd',
                              'running':  true,
                            },
                          ], })
          end

          it {
            is_expected.to contain_concat('/usr/local/bin/validate.sh').with_ensure('present')
            is_expected.to contain_concat__fragment('bash-header')
            is_expected.to contain_concat__fragment('bash-footer')
            is_expected.to contain_concat__fragment('validate-processes').with_content(%r{pgrep ntpd})
            is_expected.to contain_concat__fragment('validate-processes').with_content(%r{\$? -eq 0})
          }

          context 'with stopped process' do
            let :params do
              super().merge({ 'processes' => [
                              {
                                'name':     'ntpd',
                                'running':  false,
                              },
                            ], })
            end

            it {
              is_expected.to contain_concat__fragment('validate-processes').with_content(%r{\$? -eq 1})
            }
          end
        end

        context 'with mounts' do
          let :params do
            super().merge({ 'mounts' => [
                            '/mnt/data',
                          ], })
          end

          it {
            is_expected.to contain_concat('/usr/local/bin/validate.sh').with_ensure('present')
            is_expected.to contain_concat__fragment('bash-header')
            is_expected.to contain_concat__fragment('bash-footer')
            is_expected.to contain_concat__fragment('validate-mounts').with_content(%r{\^\/mnt\/data\$})
          }
        end

        context 'with exports' do
          let :params do
            super().merge({ 'exports' => [
                            '/share/data',
                          ], })
          end

          it {
            is_expected.to contain_concat('/usr/local/bin/validate.sh').with_ensure('present')
            is_expected.to contain_concat__fragment('bash-header')
            is_expected.to contain_concat__fragment('bash-footer')
            is_expected.to contain_concat__fragment('validate-exports').with_content(%r{\^\/share\/data})
          }
        end

        context 'with zfs_pools' do
          let :params do
            super().merge({ 'zfs_pools' => [
                            'pool2',
                          ], })
          end

          it {
            is_expected.to contain_concat('/usr/local/bin/validate.sh').with_ensure('present')
            is_expected.to contain_concat__fragment('bash-header')
            is_expected.to contain_concat__fragment('bash-footer')
            is_expected.to contain_concat__fragment('validate-zpools').with_content(%r{\^pool2\\s\+\(ONLINE\|DEGRADED\)\$})
          }
        end
      end # with ensure=>present
      it { is_expected.to compile }
    end
  end
end
