# frozen_string_literal: true

require 'spec_helper'

describe 'server_patching' do
  on_supported_os(get_distros).each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it {
        is_expected.to compile
        is_expected.to contain_class('server_patching::validate')
      }
    end
  end
end
