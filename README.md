# server_patching

The module provides classes built around server patching procedures.

## Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with server_patching](#setup)
    * [Setup requirements](#setup-requirements)
    * [Beginning with server_patching](#beginning-with-server_patching)
3. [Usage - Configuration options and additional functionality](#usage)
4. [Limitations - OS compatibility, etc.](#limitations)

## Description

At the moment the only useable class is `server_patching::validation`, which
allows to define post-patching checks and generate a bash script implementing
these checks. Designed to be invoked with AWS SSM software. 

## Setup

### Setup Requirements

The module uses `netstat`, `curl` and `pgrep`. These utilities have to be
installed on the system. Designed to work only with modern systems managed
by systemd.


### Beginning with server_patching

Commonly the module is invoked by including a subclass. For patching validation
it is just

```
include server_patching::validate
```

Further configuration is more conveniently performed in hiera.

## Usage

The following example covers the use of all module parameters:

```yaml
server_patching::validate::ensure: present
server_patching::validate::validation_script: /usr/local/sbin/validate.sh
server_patching::validate::services:
  - name: open-vm-tools.service
    active: true
  - name: openipmi.service
    active: false
server_patching::validate::processes:
  - name: falcond
    running: true
  - name: java
    command: solr 
server_patching::validate::urls:
  - url: https://netdb.stanford.edu/status-DLK87ufdskjf
    status: 200
  - url: https://netdb.stanford.edu
    resolve_to: 171.67.5.154
    status: 302
server_patching::validate::ports:
  - port: 22
    proto: tcp
    ip_ver: ipv4
    listening: true
  - port: 23
    proto: tcp
    ip_ver: ipv4
    listening: false
server_patching::validate::mounts:
  - /home
  - /mnt/data
server_patching::validate::exports:
  - /share/raw_data
  - /share/processed_data
server_patching::validate::zfs_pools:
  - pool1
  - pool2
server_patching::validate::files:
  - path: /var/log/application.log
    exists: true
    type: regular file # file types are as outputted by stat(1)
    owner: application
    group: operations
    mode: 644
    age: 5m # maximum modification time format XXdXXhXXmXXs with any omited
    min_size: 10
    max_size: 6M
    contains:
      - application startup success
      - connection established
    not_contains:
      - startup failed
      - exception 24B70 in object.getConnected
    last_lines: 100 # or 'first_lines: 100', but not both
  - path: /srv/data/project
    type: directory
    owner: pete
    group: staff
    min_size: 5M # allocated space, calculation may take time
    max_size: 10G # allocated space, calculation may take time
```

The fragment generates verifies if:
* VMWare tools service is running
* CrowdStrike daemon process is running
* NetDB URL redirects somewhere (to WebAuth presumably)
* NetDB status URL returns success
* SSH port is open over IPv4
* Telnet port is closed over IPv4
* Given ZFS pools are imported
* Given NFS shares are exported
* Verfies that application log file
    * exists
    * is a regular file ("directory" type is supported as well)
    * has correct permissions
    * has been modified within last 5 min
    * larger than 10 bytes, but smaller than 6MB
    * contains both strings "application startup success" and "connection established" (Perl regular expressions supported)
    * contains neither "startup failed" nor "exception 24B70 in object.getConnectd"
    * only last 100 lines in the file are searched for these strings
* Checks if a project directory:
    * exists
    * has correct premissions
    * has an allocated size between 5MB and 10GB
    * (text searches within directory files not supported)

The module would generate a validation script `validation.sh` in
`/usr/local/bin/` directory, which can be triggered by SSM and return a zero
exit code if all checks pass or non-zero code with a number of failed checks. It
outputs the log of the checks run to stdout.

## Limitations

Designed for use only with the systems managed by systemd.

