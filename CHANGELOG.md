# Changelog

All notable changes to this project will be documented in this file.

## Release 0.4.0

* Added file/directory check.

## Release 0.3.1

* Fixed a bug with long process names.

## Release 0.3.0

* Added resolve_to option to URL check.
* Added command option to process check.

## Release 0.2.0

* Added checks for NFS exports and ZFS pools.

## Release 0.1.0

* Initial release.
